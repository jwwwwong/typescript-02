import createOrder, { CreateOrderParams } from '../create-order'
import broadcastOrder from '../broadcast-order'
import Order from '../../models/Order'
import SwitcheoAccount from '../../switcheo/switcheo-account'
import SwitcheoConfig from '../../switcheo/switcheo-config'

export type SaveOrderParams = CreateOrderParams

export default async function saveOrder(orderParams: SaveOrderParams,
  account: SwitcheoAccount, config: SwitcheoConfig): Promise<Order> {
  const order = await createOrder(orderParams, account, config)
  return broadcastOrder(order, account, config)
}
