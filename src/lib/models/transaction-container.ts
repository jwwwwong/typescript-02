import Transaction from './transaction'

export default interface TransactionContainer {
  id: string
  transaction: Transaction
}
