import Transaction from './transaction'

export default interface TxnContainer {
  id: string
  txn: Transaction
}
